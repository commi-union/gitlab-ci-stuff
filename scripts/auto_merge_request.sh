#!/bin/bash
# Taken from: https://stackoverflow.com/questions/51104622/how-to-create-a-merge-request-at-the-end-of-a-successful-pipeline-in-gitlab
# This script was adapted from:
# https://about.gitlab.com/2017/09/05/how-to-automatically-create-a-new-mr-on-gitlab-with-gitlab-ci/

echo "Variables:"
echo "_AMR_ACCESS_TOKEN: $([ $_AMR_ACCESS_TOKEN ] && echo "SECRET" || echo "MISSING")"
echo "_AMR_API_V4_URL: $_AMR_API_V4_URL"
echo "_AMR_MERGE_REQUEST_TITLE: $_AMR_MERGE_REQUEST_TITLE"
echo "_AMR_PROJECT_ID: $_AMR_PROJECT_ID"
echo "_AMR_SOURCE_BRANCH: $_AMR_SOURCE_BRANCH"
echo "_AMR_TARGET_BRANCH: $_AMR_TARGET_BRANCH"
echo " "

effective_title=$([ $_AMR_MERGE_REQUEST_TITLE ] \
  && echo "$_AMR_MERGE_REQUEST_TITLE" \
  || echo "Auto Merge Request ($_AMR_SOURCE_BRANCH -> $_AMR_TARGET_BRANCH)")
echo "Effective Title: '$effective_title'"
echo " "

query=$(echo "
  source_branch = $_AMR_SOURCE_BRANCH &
  target_branch = $_AMR_TARGET_BRANCH &
  state         = opened
" | tr -d "[:space:]")
echo "Query: $query"
echo " "

find_response_code=$(curl -s -o find_response.txt -w "%{http_code}" \
  $_AMR_API_V4_URL/projects/$_AMR_PROJECT_ID/merge_requests?$query \
  --header "Authorization: Bearer $_AMR_ACCESS_TOKEN")

if [ $find_response_code -ge 400 ]; then
  echo "Find Failure: $find_response_code"
  cat find_response.txt
  exit 1
fi

found=$(cat find_response.txt | jq -r '.[].web_url')
echo "Found:"
echo "$found"
echo " "

if [ "$found" = "" ]; then
  body=$(echo "{
    \"source_branch\": \"$_AMR_SOURCE_BRANCH\",
    \"target_branch\": \"$_AMR_TARGET_BRANCH\",
    \"title\": \"$effective_title\"
  }" | jq '.')
  echo "Body:"
  echo "$body"
  echo " "

  create_response_code=$(curl -s -o create_response.txt -w "%{http_code}" -X POST \
    $CI_API_V4_URL/projects/$CI_PROJECT_ID/merge_requests \
    --header "Authorization: Bearer $_AMR_ACCESS_TOKEN" \
    --header "Content-Type: application/json" \
    --data "$body")

  if [ $create_response_code -ge 400 ]; then
    echo "Create Failure: $create_response_code"
    cat create_response.txt
    exit 1
  fi

  created=$(cat create_response.txt | jq -r '.web_url')
  echo "Created:"
  echo "$created"
  echo " "
fi
