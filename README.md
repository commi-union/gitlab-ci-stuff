# Gitlab CI Stuff

This repo contains reusable jobs for Gitlab CI. They are to be reworked into
[CI/CD Components](https://docs.gitlab.com/ee/ci/components/) once this feature become more widespread.

## Jobs

### Auto Merge Request

Automatically creates Merge Request (if not exists) when change happens in certain branch. Target branch is specified
as a parameter. Only activates if pipeline is triggered from branch and target branch is specified.

#### How To Enable (Gitlab v16+)

1. Add this section to your Gitlab CI yaml:

```yaml
include:
  - remote: 'https://gitlab.com/commi-union/gitlab-ci-stuff/-/raw/master/jobs/.auto_merge_request.yml'
    inputs:
      project_access_token: $AUTO_MR_ACCESS_TOKEN # Used to create MR in the project.
      #
      # Recommended way it to define it as a variable with environment scope
      # and name environments after source branches.
      target_branch: $AUTO_MR_TARGET_BRANCH

create_merge_request:
  environment: $CI_COMMIT_BRANCH
```

2. Specify variables for your project:

- AUTO_MR_ACCESS_TOKEN - should be a Gitlab token capable of creating Merge Requests in your project. Recommended to be protetcted.
- AUTO_MR_TARGET_BRANCH - variable that will define to which branch MRs should be created. This is recommended to be environment-specific.

#### How To Enable (Gitlab v15-)

Gitlab v15 does not support __spec.inputs__. Follow the instruction for Gitlab v16+, but use this snippet instead:

```yaml
include:
  - remote: 'https://gitlab.com/commi-union/gitlab-ci-stuff/-/raw/gitlab15/jobs/.auto_merge_request.yml'

create_merge_request:
  environment: $CI_COMMIT_BRANCH
  variables:
    _AMR_INPUTS_PROJECT_ACCESS_TOKEN: $AUTO_MR_ACCESS_TOKEN
    _AMR_INPUTS_TARGET_BRANCH: $AUTO_MR_TARGET_BRANCH
```

#### Examples

See [.gitlab-ci.yml]() for usage example.

### Enforce Code Flow

TODO
